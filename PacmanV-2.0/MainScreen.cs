﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace PacmanV_2._0
{
    public partial class MainScreen : Form
    {
        public MainScreen()
        {
            InitializeComponent();
            Graphics graphics = panel1.CreateGraphics();
            DrawFood();
        }
        Keys keyPressed = Keys.None;
        int timesOpened = 0;
        public static Logic logic = new Logic(); // grid                                       //Monster[] monsters = null; // ... monsters
        public void Draw(Graphics graphics, int x, int y)
        {
            Pen pen = new Pen(Color.Orange);
            graphics.DrawRectangle(pen, (panel1.Width / 28f) * x + 6.2f, (panel1.Height / 31f) * y + 6.2f, 5, 5);
        }                                                    // Pacman pacman = ...;
        public void DrawFood()
        {
            Graphics graphics = panel1.CreateGraphics();
            for (int i =0; i < logic.grid.GetLength(0); i++)
            {
                for(int j = 0; j < logic.grid.GetLength(1); j++)
                {
                    if(logic.grid[i,j] == PT.PS) 
                    {
                        Draw(graphics, i, j);
                    }
                }
            }
        }
        private void BackgroudMusicPlayer()
        {
            SoundPlayer player = new SoundPlayer(@"..\..\pacman_beginning.wav");
            player.Play();
        }

        protected override void OnPreviewKeyDown(PreviewKeyDownEventArgs e)
        {
            if (directions.Contains(e.KeyData) && logic.canPacmanProceedOnItsDirection(logic.pacman, KeyDrct[e.KeyData]))
                logic.pacmanDrct = KeyDrct[e.KeyData];

            else if (e.KeyData == Keys.F9)
            {
                Close();
                StartScreenFromProvider.StartScreen.Show();
            }
            else
            {
                if (directions.Contains(e.KeyData))
                {
                    keyPressed = e.KeyData;
                }
            }
        }
        readonly List<Keys> directions = new List<Keys>() { Keys.Up, Keys.Down, Keys.Left, Keys.Right };
        readonly Dictionary<Keys, Direction> KeyDrct = new Dictionary<Keys, Direction>() { { Keys.Up, Direction.UP },
                                                                                   { Keys.Down, Direction.DOWN },
                                                                                    { Keys.Left, Direction.LEFT },
                                                                                    { Keys.Right, Direction.RIGHT } };
        
        private void FiveTimesASecond(object sender, EventArgs e)
        {
            // ask each monster where she would like to go
            // ask pacman where he would like to go (send 'Keys' so pacman knows which direction user wants him to go)
            // check if something interestingd happened (state changed, pacman died, ...)
            // paint the whole thing:

            // lets move down:
            (int x, int y) prevLoc = logic.pacman;
            if (keyPressed != Keys.None && logic.canPacmanProceedOnItsDirection(logic.pacman, KeyDrct[keyPressed]))
            {
                logic.pacmanDrct = KeyDrct[keyPressed];
                keyPressed = Keys.None;
            }
            logic.movePacman();
            (int x, int y) currLoc = logic.pacman;
            if(logic.grid[currLoc.x, currLoc.y] == PT.PS) 
            {
                logic.FoodEeatn(currLoc.x, currLoc.y);
            }
            if(logic.Food == 0)
            {
                PlayAgain_Button.Enabled = true;
                Back_Button.Enabled = true;
                timer1.Stop();
                GameOver();
            }
            RectangleF blackPatch = ActualPixelsOfCellInTheMatrix(prevLoc.x, prevLoc.y);
            RectangleF pacmanPatch = ActualPixelsOfCellInTheMatrix(currLoc.x, currLoc.y);
            
            Graphics g = panel1.CreateGraphics();
            DrawFood();
            g.FillRectangle(blackBrush, blackPatch);   
            AnimatePacman(logic.pacmanDrct, pacmanPatch, g);
            
        }
        public void GameOver()
        {
            GameOver_Pic.Visible = true;
            PlayAgain_Button.Visible = true;
            Back_Button.Visible = true;
        }
        private readonly Brush blackBrush = new Pen(Color.Black).Brush;
        
        private RectangleF ActualPixelsOfCellInTheMatrix(int x, int y)
        {
            float width = panel1.Width / 28f;
            float height = panel1.Height / 31f;

            float startX = x * width, startY = y * height;

            return new RectangleF(startX, startY, width, height);
        }

        public void AnimatePacman(Direction pacmanDrct, RectangleF pacmanPatch, Graphics g)
        {
            if(pacmanDrct == Direction.None) 
            {
                Image img = Properties.Resources.Pacman_0;
                g.DrawImage(img, pacmanPatch);
            }
            else if (pacmanDrct == Direction.UP)
            {
                if (timesOpened == 0 && pacmanDrct == Direction.UP)
                {
                    Image img = Properties.Resources.Pacman_0;
                    g.DrawImage(img, pacmanPatch);

                }
                else if (timesOpened == 1 && pacmanDrct == Direction.UP)
                {
                    Image imag = Properties.Resources.Pacman_1_1;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 2 && pacmanDrct == Direction.UP)
                {
                    Image imag = Properties.Resources.Pacman_1_2;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 3 && pacmanDrct == Direction.UP)
                {
                    Image imag = Properties.Resources.Pacman_1_1;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 4 && pacmanDrct == Direction.UP)
                {
                    Image img = Properties.Resources.Pacman_0;
                    g.DrawImage(img, pacmanPatch);
                    timesOpened = -1;
                }
                timesOpened += 1;
            }
            else if (pacmanDrct == Direction.DOWN)
            {
                if (timesOpened == 0 && pacmanDrct == Direction.DOWN)
                {
                    Image img = Properties.Resources.Pacman_0;
                    g.DrawImage(img, pacmanPatch);

                }
                else if (timesOpened == 1 && pacmanDrct == Direction.DOWN)
                {
                    Image imag = Properties.Resources.Pacman_3_1;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 2 && pacmanDrct == Direction.DOWN)
                {
                    Image imag = Properties.Resources.Pacman_3_2;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 3 && pacmanDrct == Direction.DOWN)
                {
                    Image imag = Properties.Resources.Pacman_3_1;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 4 && pacmanDrct == Direction.DOWN)
                {
                    Image img = Properties.Resources.Pacman_0;
                    g.DrawImage(img, pacmanPatch);
                    timesOpened = -1;
                }

                timesOpened += 1;
            }
            else if (pacmanDrct == Direction.RIGHT)
            {
                if (timesOpened == 0 && pacmanDrct == Direction.RIGHT)
                {
                    Image img = Properties.Resources.Pacman_0;
                    g.DrawImage(img, pacmanPatch);

                }
                else if (timesOpened == 1 && pacmanDrct == Direction.RIGHT)
                {
                    Image imag = Properties.Resources.Pacman_2_1;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 2 && pacmanDrct == Direction.RIGHT)
                {
                    Image imag = Properties.Resources.Pacman_2_2;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 3 && pacmanDrct == Direction.RIGHT)
                {
                    Image imag = Properties.Resources.Pacman_2_1;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 4 && pacmanDrct == Direction.RIGHT)
                {
                    Image img = Properties.Resources.Pacman_0;
                    g.DrawImage(img, pacmanPatch);
                    timesOpened = -1;
                }

                timesOpened += 1;
            }
            else if (pacmanDrct == Direction.LEFT)
            {
                if (timesOpened == 0 && pacmanDrct == Direction.LEFT)
                {
                    Image img = Properties.Resources.Pacman_0;
                    g.DrawImage(img, pacmanPatch);

                }
                else if (timesOpened == 1 && pacmanDrct == Direction.LEFT)
                {
                    Image imag = Properties.Resources.Pacman_4_1;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 2 && pacmanDrct == Direction.LEFT)
                {
                    Image imag = Properties.Resources.Pacman_4_2;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 3 && pacmanDrct == Direction.LEFT)
                {
                    Image imag = Properties.Resources.Pacman_4_1;
                    g.DrawImage(imag, pacmanPatch);
                }
                else if (timesOpened == 4 && pacmanDrct == Direction.LEFT)
                {
                    Image img = Properties.Resources.Pacman_0;
                    g.DrawImage(img, pacmanPatch);
                    timesOpened = -1;
                }

                timesOpened += 1;
            }

        }

        private void MainScreen_Load(object sender, EventArgs e)
        { 
            BackgroudMusicPlayer();
        }
        public void ClearBoard()
        {
            Graphics graphics = panel1.CreateGraphics();
            Brush pen = new Pen(Color.Black).Brush;
            for (int i = 0; i < logic.grid.GetLength(0); i++)
            {
                for (int j = 0; j < logic.grid.GetLength(1); j++)
                {
                    if (logic.grid[i, j] == PT.FD || logic.grid[i, j] == PT.PS)
                    {
                        logic.grid[i, j] = PT.PS;
                        graphics.FillRectangle(pen, ActualPixelsOfCellInTheMatrix(i,j));
                    }
                }
            }
        }
        private void PlayAgain_Button_Click(object sender, EventArgs e)
        {
            logic = new Logic();
            //BackgroudMusicPlayer();       
            ClearBoard();
            GameOver_Pic.Visible = false;
            PlayAgain_Button.Visible = false;
            Back_Button.Visible = false;
            PlayAgain_Button.Enabled = false;
            Back_Button.Enabled = false;
            DrawFood();
            timer1.Start();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            logic = new Logic();
            ClearBoard();
            DrawFood(); 
            Close();
            StartScreenFromProvider.StartScreen.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            logic = new Logic();
            ClearBoard();
            DrawFood();
            Close();
            StartScreenFromProvider.StartScreen.Show(); 
        }
    }
}
/*


   

    צפה בסרטון של ההתנהגות של המפלצות
     */
