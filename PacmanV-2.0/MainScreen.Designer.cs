﻿namespace PacmanV_2._0
{
    partial class MainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainScreen));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Back_Button = new System.Windows.Forms.Button();
            this.PlayAgain_Button = new System.Windows.Forms.Button();
            this.GameOver_Pic = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GameOver_Pic)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 170;
            this.timer1.Tick += new System.EventHandler(this.FiveTimesASecond);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(16, 689);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(209, 20);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.Back_Button);
            this.panel1.Controls.Add(this.PlayAgain_Button);
            this.panel1.Controls.Add(this.GameOver_Pic);
            this.panel1.Location = new System.Drawing.Point(13, 11);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 665);
            this.panel1.TabIndex = 0;
            // 
            // Back_Button
            // 
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Location = new System.Drawing.Point(332, 460);
            this.Back_Button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(111, 34);
            this.Back_Button.TabIndex = 2;
            this.Back_Button.Text = "Back";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Visible = false;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // PlayAgain_Button
            // 
            this.PlayAgain_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.PlayAgain_Button.Location = new System.Drawing.Point(332, 417);
            this.PlayAgain_Button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PlayAgain_Button.Name = "PlayAgain_Button";
            this.PlayAgain_Button.Size = new System.Drawing.Size(111, 36);
            this.PlayAgain_Button.TabIndex = 1;
            this.PlayAgain_Button.Text = "Play Again";
            this.PlayAgain_Button.UseVisualStyleBackColor = true;
            this.PlayAgain_Button.Visible = false;
            this.PlayAgain_Button.Click += new System.EventHandler(this.PlayAgain_Button_Click);
            // 
            // GameOver_Pic
            // 
            this.GameOver_Pic.BackColor = System.Drawing.Color.Transparent;
            this.GameOver_Pic.Image = global::PacmanV_2._0.Properties.Resources.GameOver1;
            this.GameOver_Pic.Location = new System.Drawing.Point(216, 208);
            this.GameOver_Pic.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.GameOver_Pic.Name = "GameOver_Pic";
            this.GameOver_Pic.Size = new System.Drawing.Size(377, 202);
            this.GameOver_Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.GameOver_Pic.TabIndex = 0;
            this.GameOver_Pic.TabStop = false;
            this.GameOver_Pic.Visible = false;
            // 
            // MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(829, 720);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainScreen";
            this.Load += new System.EventHandler(this.MainScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GameOver_Pic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox GameOver_Pic;
        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Button PlayAgain_Button;
    }
}