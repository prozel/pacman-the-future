﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanV_2._0
{
    public abstract class Monster
    {
        public (int x, int y) location, target; // מיקומים במערך
        public Direction currentMovingDirection;

        public static double Distance((int x, int y) location, (int x, int y) target)
        {
            return Math.Sqrt((location.x - target.x) * (location.x - target.x) + (location.y - target.y) * (location.y - target.y));
        }

        public abstract Direction WhereToMove(State state);
    }

    class A_star
    {
        public static List<(int x, int y)> findShortestPath((int x, int y) start, (int x, int y) target, Func<(int x, int y), List<(int x, int y)>> neighbors)
        {
            List<(int x, int y)> OPEN = new List<(int x, int y)>(); // todo it actually should be a heap, not a list
            HashSet<(int x, int y)> CLOSED = new HashSet<(int x, int y)>(); // HashSet is similar to List, but can perform 'Contains' check in O(1) whilst list does that in O(Nn)

            List<(int, int)> lst = neighbors((3, 40));
        }
    }

    public class PinkyMonster : Monster
    {
        public override Direction WhereToMove(State state)
        {
            double shortestDist = double.MaxValue;
            Direction bestDirection = null;

            foreach (Direction direction in Direction.ALL_DIRECTIONS)
            {
                (int x, int y) neighbor = (location.x + direction.dx, location.y + direction.dy); // one of the four cells adjacent to my location
                /*
                if (!direction.isOpposite(this.currentMovingDirection) && grid[neighbor.x, neighbor.y] != '#')
                {
                    double dist = Monster.Distance(neighbor, target);
                    if (dist < shortestDist)
                    {
                        shortestDist = dist;
                        bestDirection = direction;
                    }
                }*/
            }

            return Direction.LEFT;
        }
    }

    public enum State
    {
        Scatter, Chase, Eaten, Frightened
    }

    public class Direction
    {
        public readonly int dx, dy; // לקריאה בלבד - לא ניתן לשינוי אחר כך
        private Direction(int dx, int dy)
        {
            this.dx = dx;
            this.dy = dy;
        }

        public static readonly Direction
            UP = new Direction(dx: 0, dy: -1),
            DOWN = new Direction(0, 1),
            LEFT = new Direction(-1, 0),
            RIGHT = new Direction(1, 0),
            None = new Direction(0, 0);

        public static readonly Direction[] ALL_DIRECTIONS = { UP, DOWN, LEFT, RIGHT };

        public bool IsOpposite(Direction other)
        {
            if (this.dx == -other.dx && this.dy == -other.dy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
