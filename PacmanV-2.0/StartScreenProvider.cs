﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanV_2._0
{
    class StartScreenFromProvider
    {
        public static StartScreen StartScreen
        {
            get
            {
                if (_mainMenu == null)
                {
                    _mainMenu = new StartScreen();
                }
                return _mainMenu;
            }
        }
        private static StartScreen _mainMenu;
    }
}
