﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanV_2._0
{
    public class Logic
    {
        public int Food = 300;
        public PT[,] grid = {
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.MP,PT.MP,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.MH,PT.MH,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.WL,PT.MH,PT.MH,PT.WL,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL },
                        { PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL,PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.PS,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.PS,PT.WL },
                        { PT.WL,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.PS,PT.WL },
                        { PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL,PT.WL } };

        private static PT[,] Transpose(PT[,] matrix)
        {
            int w = matrix.GetLength(0);
            int h = matrix.GetLength(1);

            PT[,] result = new PT[h, w];

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    result[j, i] = matrix[i, j];
                }
            }

            return result;
        }

        public Logic()
        {
            grid = Transpose(grid);
        }

        public (int x, int y) pacman = (13, 23);

        public Direction pacmanDrct = Direction.None;

        public bool canPacmanProceedOnItsDirection((int x, int y) pacman, Direction pacmanDrct) // todo  ? don't pass pacmanDrct to the function... use this.pacmanDrct inside
        {
            if (pacman == (0, 14) && pacmanDrct == Direction.LEFT || pacman == (27, 14) && pacmanDrct == Direction.RIGHT)
                return true;
            (int x, int y) nextLoc = (pacman.x + pacmanDrct.dx, pacman.y + pacmanDrct.dy);
            if (grid[nextLoc.x, nextLoc.y] == PT.PS || grid[nextLoc.x, nextLoc.y] == PT.FD)
                return true;
            else
                return false;
        }
        

        public void movePacman()
        {
            if (canPacmanProceedOnItsDirection(pacman, pacmanDrct))
            {
                if (pacman == (0, 14) && pacmanDrct == Direction.LEFT)
                    pacman = (27, 14);
                else if (pacman == (27, 14) && pacmanDrct == Direction.RIGHT)
                    pacman = (0, 14);
                else
                    pacman = (pacman.x + pacmanDrct.dx, pacman.y + pacmanDrct.dy);
            }
        }
        public void FoodEeatn(int x, int y) 
        {
            Food--;
            grid[x, y] = PT.FD;
        }
    }


    public enum PT
    {
        WL, PS, MH, MP, FD
    }
}
