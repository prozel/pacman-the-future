﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace PacmanV_2._0
{
    public partial class StartScreen : Form
    {
        public readonly SoundPlayer player = new SoundPlayer(@"C:\Users\poleg kashti\Documents\Pacman\pacmanMusic.wav");
        public StartScreen()
        {
            InitializeComponent();
        }
        protected override void OnPreviewKeyDown(PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                player.Stop();
                MainScreen Ms = new MainScreen();
                Ms.Show();
                Hide();
            }
            if (e.KeyData == Keys.F12)
            {
                player.Stop();
                this.Close();
            }

        }
        private void StartScreen_Load_1(object sender, EventArgs e)
        {
            player.PlayLooping();
        }

        private void PictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This Game Created By \nPoleg Kashti And Nitsan.\nAll rights Reserved-2020", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void PictureBox5_Click(object sender, EventArgs e)
        {
            Settings_Form inf = new Settings_Form();
            inf.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            player.Stop();
            MainScreen Ms = new MainScreen();
            Ms.Show();
            Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            player.Stop();
            this.Close();
        }
    }
}
